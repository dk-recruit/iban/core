package fr.darkonia.core.sql;

import fr.darkonia.core.*;

public enum DbManager {

	BD(new DbCredentials(Core.getInstance().getConfig().getString("database.host"),
			Core.getInstance().getConfig().getString("database.user"),
			Core.getInstance().getConfig().getString("database.pass"),
			Core.getInstance().getConfig().getString("database.dbName"), 3306));
	
	private DbAccess dbAccess;
	
	DbManager(DbCredentials credentials){
		this.dbAccess = new DbAccess(credentials);
	}
	
	public DbAccess getDbAccess(){
		return dbAccess;
	}
	
	
	public static void initAllDbConnections(){
		for(final DbManager dbManager : values()){
			dbManager.dbAccess.initPool();
		}
	}
	
	public static void closeAllDbConnections(){
		for(final DbManager dbManager : values()){
			dbManager.dbAccess.closePool();
		}
	}
}

