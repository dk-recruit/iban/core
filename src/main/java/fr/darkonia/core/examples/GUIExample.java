package fr.darkonia.core.examples;

import java.util.function.Supplier;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import fr.darkonia.core.ItemCreator;
import fr.darkonia.core.gui.CustomInventory;

public class GUIExample implements CustomInventory {
	
	/*
	 * 1 - Créer une classe
	 * 2 - Implémenter CustomInventory
	 * 3 - Remplir les méthodes comme souhaité
	 * 4 - GUI.addMenu(new NomDeLaClasse()); dans le onEnable()
	 * 5 - Pour ouvrir l'inventaire : GUI.open(joueur, NomDeLaClasse.class);
	 */

	@Override
	public String getName() {
		return "§aInventaire blablabla";
	}

	//Cancel ou pas le InventoryClickEvent
	@Override
	public boolean getCancelled() {
		return true;
	}

	
	//Contenu de l'inventaire
	@Override
	public Supplier<ItemStack[]> getContents(Player player) {
        ItemStack[] slots = new ItemStack[getSlots()];
        
        slots[20] = new ItemCreator(Material.DIAMOND_SWORD).setName("§5§lTest").addLore("§eCeci est un lore").getItem();
        
        return () -> slots;
	}

	//Appelé quand l'event InventoryClickEvent est appelé
	@Override
	public void onClick(Player player, Inventory inventory, ItemStack clickedItem, int slot, boolean isLeftClick,
			boolean isRightclick) {
		
		if(clickedItem.getType() == Material.DIAMOND_SWORD) {
			player.sendMessage("§aVous avez cliqué sur une épée en diams !");
		}
		
	}

	
	//Appelé quand l'event InventoryCloseEvent est appelé
	@Override
	public void onClose(Player p, Inventory inv) {
		
	}

	//Nombre de lignes dans le GUI
	@Override
	public int getRows() {
		return 1;
	}

}
