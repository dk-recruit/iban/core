package fr.darkonia.core;

import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import fr.darkonia.core.examples.GUIExample;
import fr.darkonia.core.gui.GUI;
import fr.darkonia.core.gui.InventoryListener;
import fr.darkonia.core.sql.DbManager;
import lombok.Getter;

public class Core extends JavaPlugin {
	
	@Getter
	private static Core instance;
	
	@Override
	public void onEnable() {
		instance = this;
		new GUI();
		saveDefaultConfig();
		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(new InventoryListener(), this);
		GUI.addMenu(new GUIExample());
		DbManager.initAllDbConnections();
	}
	
	@Override
	public void onDisable() {
		DbManager.closeAllDbConnections();
	}
	
	@Override
	public void onLoad() {
		
	}

}
