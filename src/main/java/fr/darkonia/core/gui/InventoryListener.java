package fr.darkonia.core.gui;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;


public class InventoryListener implements Listener {

	@EventHandler
	public void onClick(InventoryClickEvent e){
		Player p = (Player) e.getWhoClicked();
		Inventory inv = e.getInventory();
		ItemStack current = e.getCurrentItem();

		if(current == null) return;

		GUI.registeredMenus.values().stream().filter(menu -> e.getView().getTitle().equalsIgnoreCase(menu.getName()))
		.forEach(menu -> {
			menu.onClick(p, inv, current, e.getSlot(), e.getClick().isLeftClick(), e.getClick().isRightClick());
			e.setCancelled(menu.getCancelled());
		});

	}

	@EventHandler
	public void onClose(InventoryCloseEvent e) {
		Player p = (Player) e.getPlayer();
		Inventory inv = e.getInventory();

		GUI.registeredMenus.values().stream().filter(menu -> e.getView().getTitle().equalsIgnoreCase(menu.getName()))
		.forEach(menu -> {
			menu.onClose(p, inv);
		});
	}

}
