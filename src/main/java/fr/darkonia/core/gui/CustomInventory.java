package fr.darkonia.core.gui;

import java.util.function.Supplier;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public interface CustomInventory {

    String getName();
    
    boolean getCancelled();

    Supplier<ItemStack[]> getContents(Player player);

    void onClick(Player player, Inventory inventory, ItemStack clickedItem, int slot, boolean isLeftClick, boolean isRightclick);
    
    void onClose(Player p, Inventory inv);

    int getRows();

    default int getSlots() {
        return getRows() * 9;
    }
}

