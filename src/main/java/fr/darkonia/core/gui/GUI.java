package fr.darkonia.core.gui;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class GUI {

	public GUI() {
		registeredMenus = new HashMap<>();
	}

	public static Map<Class<? extends CustomInventory>, CustomInventory> registeredMenus;

	public static void addMenu(CustomInventory m) {
		GUI.registeredMenus.put(m.getClass(), m);
	}


	public static void open(Player player, Class<? extends CustomInventory> gClass) {

		if (!GUI.registeredMenus.containsKey(gClass))
			return;

		CustomInventory menu = GUI.registeredMenus.get(gClass);
		Inventory inv = Bukkit.createInventory(null, menu.getSlots(), menu.getName());
		inv.setContents(menu.getContents(player).get());
		player.openInventory(inv);

	}
}
